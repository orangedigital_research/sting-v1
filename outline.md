# STNG Stack | Strapi, Terraform, Next.js, Graphql

## Exploring an modern React based website platform offering, to compete against Webflow and Wordpress

---

**For this platform to be viable as an alternative, the minimum requirements are:**

- User friendly backend for client and marketing team (create page, block, layout, upload images etc...).
- Must Open source / No licence cost.
- Cost & Time effective from a design & development standpoint.
- Easy to scale, deploy, maintain and update by the Dev Circle.
- SEO friendly (URL path, page speed, editable metatag).

Nice to haves:

- Front-end editable.
- Per page publication / deployment (Incremental Static Regeneration (ISR)).

To give you a benchmark:

- $15-$25k for **Webflow** (80-100h dev+design)
- $35-$45k for **Wordpress** (200h dev+design)
