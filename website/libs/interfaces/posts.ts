export interface PostsResponse {
  posts: Post[]
}

export type PostProps = Post

export interface Post {
  title: string
  slug: string
  author: Author
  content: string
}

interface Author {
  username: string
}

export interface PostResponse {
  postsConnection: PostsConnection
}

interface PostsConnection {
  values: Post[]
}
