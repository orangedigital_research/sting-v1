import { ApolloClient, InMemoryCache } from '@apollo/client'

const API_URL = process.env.STRAPI_GQL_DEV

export const client = new ApolloClient({
  uri: API_URL,
  cache: new InMemoryCache(),
})
