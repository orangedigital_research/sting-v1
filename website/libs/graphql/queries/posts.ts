import { gql } from '@apollo/client'

export const GET_POSTS = gql`
  query GetPosts {
    posts {
      title
      slug
      author {
        username
      }
      content
    }
  }
`

export const GET_POST = gql`
  query GetPost($slug: String!) {
    postsConnection(where: { slug: $slug }) {
      values {
        title
        slug
        author {
          username
        }
        content
      }
    }
  }
`
