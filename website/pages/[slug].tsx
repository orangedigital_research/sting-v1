import type { NextPage } from 'next'
import { GetStaticProps, GetStaticPaths } from 'next'
import Head from 'next/head'
import { client } from '@stng/graphql'
import { GET_POSTS, GET_POST } from '@stng/graphql/queries'
import { PostProps, PostsResponse, PostResponse } from '@stng/interfaces'
import { Heading, VStack, Text } from '@chakra-ui/layout'
import { ParsedUrlQuery } from 'querystring'

const PostPage: NextPage<PostProps> = (props) => {
  const { title, content, author } = props
  console.log({ props })

  return (
    <div>
      <Head>
        <title>Website</title>
      </Head>

      <VStack spacing={5}>
        <Heading size="sm">{title}</Heading>
        <Heading size="xs">by {author.username}</Heading>
        <Text as="p" dangerouslySetInnerHTML={{ __html: content }}></Text>
      </VStack>
    </div>
  )
}

export default PostPage

export const getStaticPaths: GetStaticPaths = async () => {
  const { data } = await client.query<PostsResponse>({
    query: GET_POSTS,
  })

  const paths = data.posts.map((post) => ({ params: { slug: post.slug } }))

  return {
    paths,
    fallback: true,
  }
}

interface Params extends ParsedUrlQuery {
  slug: string
}

export const getStaticProps: GetStaticProps<any, Params> = async ({ params }) => {
  const slug = params!.slug

  const { data } = await client.query<PostResponse>({
    query: GET_POST,
    variables: { slug },
  })

  return {
    props: data.postsConnection.values[0],
  }
}
