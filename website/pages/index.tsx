import type { NextPage } from 'next'
import { GetStaticProps } from 'next'
import Head from 'next/head'
import { client } from '@stng/graphql'
import { GET_POSTS } from '@stng/graphql/queries'
import { Post, PostProps, PostsResponse } from '@stng/interfaces'
import { Heading, VStack, Text, Link } from '@chakra-ui/layout'

export interface HomePageProps {
  posts: Post[]
}

const Home: NextPage<HomePageProps> = (props) => {
  const { posts } = props
  return (
    <div>
      <Head>
        <title>Website</title>
      </Head>

      <VStack spacing={5}>
        {posts.map((post) => (
          <VStack key={post.slug} spacing={2}>
            <Heading size="sm">{post.title}</Heading>
            <Heading size="xs">by {post.author.username}</Heading>
            <Text as="p" dangerouslySetInnerHTML={{ __html: post.content }}></Text>
            <Link color="blue.500" href={`http://localhost:3000/${post.slug}`} isExternal>
              &rarr; to post
            </Link>
          </VStack>
        ))}
      </VStack>
    </div>
  )
}

export default Home

export const getStaticProps: GetStaticProps = async () => {
  const { data } = await client.query<PostsResponse>({
    query: GET_POSTS,
  })

  return {
    props: { posts: data.posts },
  }
}
