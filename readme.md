# STNG Stack | Strapi, Terraform, Next.js, Graphql

## Exploring an modern React based website platform offering, to compete against Webflow and Wordpress

---

to be continued...

---

## Strapi Configuration

1. install strapi
2. instal graphql plugin
3. create content types
4. Allow public read access (settings > users & permissions plugin > roles)

notes:

- **grapql** : `http://localhost:1337/graphql`
- **admin** : `http://localhost:1337/admin`
